import readline from "readline"

export const read = async ():Promise<number> => {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    })

    const start = async () => {
      for await (const line of rl) {
        return line
      }
    }

  return Number(await start())
}