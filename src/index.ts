// import { all } from "./foo"

import { Car } from "./car"
import { Driver } from "./driver"
import { rase } from "./foo"

// let str:string ="Текст"
// str ="Текст2"
// let num: number = 4
// let bool: boolean =true
// bool =false
// all(6,3)
// import { culc2 } from "./foo";
// const anw1 =culc2(5,3)
// console.log(anw1)
// const anw2 =culc2(2,7)
// console.log(anw2)
// const anw3 =culc2(4,4)
// console.log(anw3)
// import { culc } from "./foo";
// culc()
const driver1 = new Driver("Петя", 100)
const driver2 = new Driver("Вася", 240)
const car = new Car("Зелёный", "Пикап", 220, driver1)
car.drive()
car.repaint("Синий")
const car2 = new Car("Фиолетовый", "Седан", 180, driver2)
console.log(rase(car, car2))
