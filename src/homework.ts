import { features } from "process"

const summ1 = function (a: number, b: number) {
  if (a < b) {
    return "7"
  } else {
    return "8"
  }
}
console.log(summ1(1, 2))

const tusk2 = function (a: number, b: number) {
  if (a > b) {
    return "yes"
  } else {
    return "no"
  }
}
console.log(tusk2(4, 2))

const tusk3 = function (a: number) {
  if (a < 3) {
    return "yes"
  } else {
    return "no"
  }
}
console.log(tusk3(1))

const tusk4 = function (a: number, b: number) {
  const c = a == b
  return c
}
console.log(tusk4(3, 7))

const tusk5 = function (a: number, b: number) {
  if (a < b) {
    return a
  } else {
    return b
  }
}
console.log(tusk5(1, 7))

const tusk6 = function (a: number, b: number) {
  if (a > b) {
    return a
  } else {
    return b
  }
}
console.log(tusk6(7, 1))

const tusk7 = function (a: number, b: number, c: number, x: number) {
  const d = a * x * x + b * x + c
  return d
}
console.log(tusk7(4, 3, 2, 1))

const tusk8 = function (a: number, b: number) {
  const c = a * a
  const d = b * b
  if (c > d) {
    return c
  } else {
    return d
  }
}
console.log(tusk8(2, 4))

const tusk9 = function (a: number, b: number, c: number) {
  if (a == b && b == c) {
    return "Треугольник равносторонний"
  }
  return "Треугольник неравносторонний"
}
console.log(tusk9(2, 2, 2))
console.log(tusk9(3, 2, 1))

const orfoo = function (a: boolean, b: boolean) {
  if (a || b) {
    return "Одно из выражений верно"
  }
  return "Выражения не верны"
}
console.log(orfoo(true, true))
console.log(orfoo(true, false))
console.log(orfoo(false, true))
console.log(orfoo(false, false))

const andfoo = function (a: boolean, b: boolean) {
  if (a && b) {
    return "Оба выражения верны"
  }
  return "Одно из выражений неверно"
}
console.log(andfoo(true, true))
console.log(andfoo(true, false))
console.log(andfoo(false, true))
console.log(andfoo(false, false))

const notfoo = function (a: boolean) {
  if (!a) {
    return "Выражение неверно"
  }
  return "Выражение верно"
}
console.log(notfoo(true))
console.log(notfoo(false))

const test = function (a: boolean, b: boolean, c: boolean) {
  return (a || b) && c
}
console.log(test(true, true, true))
console.log(test(true, false, true))
console.log(test(false, true, false))
console.log(test(true, true, false))

function task12(a: number, b: number, c: number) {
  if (a >= 0) {
    console.log(Math.pow(a, 2))
  } else {
    console.log(Math.pow(a, 4))
  }
  if (b >= 0) {
    console.log(Math.pow(b, 2))
  } else {
    console.log(Math.pow(b, 4))
  }
  if (c >= 0) {
    console.log(Math.pow(c, 2))
  } else {
    console.log(Math.pow(c, 4))
  }
}
task12(3, -2, -4)
