import { Car } from "./car"
import { Driver } from "./driver"

export function rase(a: Car, b: Car) {
  if (a.maxSpeed + a.driver.speed > b.maxSpeed + b.driver.speed) {
    return "Победил " + a.driver.name + " На машине " + a.type
  }
  return "Победил " + b.driver.name + " На машине " + b.type
}
