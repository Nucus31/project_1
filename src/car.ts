import { Driver } from "./driver"

export class Car {
  color: string
  type: string
  maxSpeed: number
  driver: Driver
  constructor(color: string, type: string, maxSpeed: number, driver: Driver) {
    this.color = color
    this.type = type
    this.maxSpeed = maxSpeed
    this.driver = driver
  }

  drive() {
    console.log("едем")
  }

  repaint(color: string) {
    this.color = color
  }
}
